/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Hydra
 */
public class dashboard {
    private static Connection connection;
    private static String query;
    public static String nama;
    public static Float hargaSatuan;
    
    public  static void Show(DefaultTableModel tableModel)
    {
        
        try 
        {
           connection = core.connect.getConnection();
           
           query = "SELECT beli.nama, beli.barangID, beli.harga, barang.hargaSatuan, barang.stok " +
                   "FROM beli, barang " +
                   "WHERE beli.barangID = barang.barangID ORDER BY barang.barangID";
           Statement statement = (Statement) connection.createStatement();
           ResultSet result = statement.executeQuery(query);
           
           Integer id=0;
           Float harga= Float.parseFloat("0");
           while (result.next()){
//                System.out.println(id +" "+ Integer.parseInt(result.getString("barangID")));
                Object[] gung = new Object[4];
                if(id == Integer.parseInt(result.getString("barangID")))
                {
                    continue;
                }
                
                
                gung[0] = result.getString("barangID");
                gung[1] = result.getString("nama");                
//                gung[2] = result.getString("harga");
                gung[2] = result.getString("hargaSatuan");
                gung[3] = result.getString("stok");
                tableModel.addRow(gung);
//                System.out.println(tableModel.);
                id = Integer.parseInt(result.getString("barangID"));
            }
           
           
            result.close();
            statement.close();
           
        }catch (SQLException ex){    
          System.out.println("Terjadi error pada saat tampil data");
        }
        
    }
     
    public  static void Search(DefaultTableModel tableModel)
    {
        
        try 
        {
           connection = core.connect.getConnection();
           
           query = "SELECT beli.nama, beli.barangID, beli.harga, barang.hargaSatuan, barang.stok " +
                   "FROM beli, barang " +
                   "WHERE beli.barangID = barang.barangID AND barang.nama LIKE '%"+ nama +"%' ORDER BY barang.barangID";
           Statement statement = (Statement) connection.createStatement();
           ResultSet result = statement.executeQuery(query);
            
          Integer id=0;
           Float harga= Float.parseFloat("0");
           while (result.next()){
//                System.out.println(id +" "+ Integer.parseInt(result.getString("barangID")));
                Object[] gung = new Object[4];
                if(id == Integer.parseInt(result.getString("barangID")))
                {
                    continue;
                }
                
                
                gung[0] = result.getString("barangID");
                gung[1] = result.getString("nama");                
//                gung[2] = result.getString("harga");
                gung[2] = result.getString("hargaSatuan");
                gung[3] = result.getString("stok");
                tableModel.addRow(gung);
//                System.out.println(tableModel.);
                id = Integer.parseInt(result.getString("barangID"));
            }
           
            result.close();
            statement.close();
           
        }catch (SQLException ex){    
          System.out.println("Terjadi error pada saat tampil data");
        }
        
    }
}
