/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author Hydra
 */
public class user {
    private static Connection connection;
    private static String query;
    
    
    public static Integer userID;
    public static String username;
    public static String password;
    public static String status;
    
    
    
    public static void save(){

        
        query = "INSERT INTO user"
                + "(userID, username, password, status)"
                + "VALUES (?,?,?,?)";
        try {
          
          connection = core.connect.getConnection();
          
          PreparedStatement statement= (PreparedStatement) connection.prepareStatement(query);
          
          statement.setInt(1, userID);
          statement.setString(2, username);
          statement.setString(3, password);
          statement.setString(4, status);
          if(statement.executeUpdate() == 1){JOptionPane.showMessageDialog(null, "Data tersimpan.");}
          else if(statement.executeUpdate() == 0) {JOptionPane.showMessageDialog(null, "Data gagal tersimpan.");}
          
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Terjadi error pada saat input data");
        } 
    }
    
    public static void update(){
        
        query = "UPDATE user SET "
            + "username=?, password=?, status=?"
            + "WHERE userID=?";
        
        try {
          
          connection = core.connect.getConnection();
          if(password == null){
              System.out.println("aas");
              PreparedStatement statement = (PreparedStatement) connection.prepareStatement("UPDATE user SET "
                + "username=?, status=? WHERE userID=?");
              
              statement.setString(1, username);
              statement.setString(2, status);
              statement.setInt(3, userID);
              if(statement.executeUpdate() == 1) {JOptionPane.showMessageDialog(null, "Data tersimpan.");}
              else if(statement.executeUpdate() == 0) {JOptionPane.showMessageDialog(null, "Data gagal tersimpan.");}
          }
          else{
              System.out.println("ass" + password);
              PreparedStatement statement = (PreparedStatement) connection.prepareStatement(query);
              statement.setString(1, username);
              statement.setString(2, password);
              statement.setString(3, status);
              statement.setInt(4, userID);
              if(statement.executeUpdate() == 1){JOptionPane.showMessageDialog(null, "Data tersimpan.");}
              else if(statement.executeUpdate() == 0) {JOptionPane.showMessageDialog(null, "Data gagal tersimpan.");}
          }
          
          
        } catch (SQLException ex){
            
         JOptionPane.showMessageDialog(null, "Terjadi error pada saat perbaru data");
        } 
    }
    
    public static void Delete()
    {
        query = "DELETE FROM user WHERE userID=?";   
        try 
        {
            connection = core.connect.getConnection();
            
            PreparedStatement statement = (PreparedStatement) connection.prepareStatement(query);
            statement.setInt(1, userID);
            if(statement.executeUpdate() == 1) {JOptionPane.showMessageDialog(null, "Data terhapus.");}
            else if(statement.executeUpdate() == 0)
            {JOptionPane.showMessageDialog(null, "Data gagal terhapus.");}
            
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Terjadi error pada saat hapus data");
        } 
    }
    
    public  static void Show(DefaultTableModel tableModel)
    {
        
        try 
        {
           connection = core.connect.getConnection();
           
           query = "SELECT * FROM user";      
           Statement statement = (Statement) connection.createStatement();
           ResultSet result = statement.executeQuery(query);
            
           while (result.next()){
                Object[] users = new Object[3];
                users[0] = result.getString("userID");
                users[1] = result.getString("username");
                users[2] = result.getString("status");
                tableModel.addRow(users);
            }
           
            result.close();
            statement.close();
           
        }catch (SQLException ex){    
          System.out.println("Terjadi error pada saat tampil data");
        }
        
    }
    
    
     public  static void FilterbyID(DefaultTableModel tableModel)
    {
        
        try 
        {
           connection = core.connect.getConnection();
           
            query = "SELECT * FROM user WHERE userID LIKE '%"+ userID +"%'";    
           Statement statement = (Statement) connection.createStatement();
           ResultSet result = statement.executeQuery(query);
            
           while (result.next()){
                Object[] users = new Object[3];
                users[0] = result.getString("userID");
                users[1] = result.getString("username");
                users[2] = result.getString("status");
                tableModel.addRow(users);
            }
           
            result.close();
            statement.close();
           
        }catch (SQLException ex){    
          System.out.println("Terjadi error pada saat tampil data");
        }
        
    }
     
      public  static ResultSet Login()
    {
        
        try 
        {
           connection = core.connect.getConnection();
           
           query = "SELECT * FROM user WHERE username=? AND password=?";    
           PreparedStatement statement = (PreparedStatement) connection.prepareStatement(query);
           statement.setString(1, username);
           statement.setString(2, password);
           ResultSet result = statement.executeQuery();
           return result;
                   
        }catch (SQLException ex){    
          return null;
        }
        
    }
}
