/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Hydra
 */
public class barang {
    private static Connection connection;
    private static String query;
    
    public static Integer barangID;
    public static String nama;
    public static Integer stok;
    public static Float hargaSatuan;
    
    public static int save(){

        
        query = "INSERT INTO barang"
                + "(barangID, nama, stok, hargaSatuan)"
                + "VALUES (?,?,?,?)";
        try { 
          
          connection = core.connect.getConnection();
          
          PreparedStatement statement= (PreparedStatement) connection.prepareStatement(query);
          
          statement.setInt(1, barangID);
          statement.setString(2, nama);
          statement.setInt(3, stok);
          statement.setFloat(4, hargaSatuan);
          return statement.executeUpdate();          
          
        } catch (SQLException ex){
            return 0;
        } 
        
    }
    
    public static void beli(){
        
        query = "UPDATE barang SET "
            + "nama=?, stok=stok+?, hargaSatuan=?"
            + "WHERE barangID=?";
        
        try {
            connection = core.connect.getConnection();
            PreparedStatement statement = (PreparedStatement) connection.prepareStatement(query);
            statement.setString(1, nama);
            statement.setInt(2, stok);
            statement.setFloat(3, hargaSatuan);
            statement.setInt(4, barangID);
            if(statement.executeUpdate() == 1){JOptionPane.showMessageDialog(null, "Data tersimpan.");}
            else if(statement.executeUpdate() == 0) {JOptionPane.showMessageDialog(null, "Data gagal tersimpan.");}
          
        } catch (SQLException ex){
            
         JOptionPane.showMessageDialog(null, "Terjadi error pada saat perbaru data");
        } 
    }
    
    public static void update(){
        
        query = "UPDATE barang SET "
            + "nama=?, stok=?, hargaSatuan=?"
            + "WHERE barangID=?";
        
        try {
            connection = core.connect.getConnection();
            PreparedStatement statement = (PreparedStatement) connection.prepareStatement(query);
            statement.setString(1, nama);
            statement.setInt(2, stok);
            statement.setFloat(3, hargaSatuan);
            statement.setInt(4, barangID);
            if(statement.executeUpdate() == 1){JOptionPane.showMessageDialog(null, "Data tersimpan.");}
            else if(statement.executeUpdate() == 0) {JOptionPane.showMessageDialog(null, "Data gagal tersimpan.");}
          
        } catch (SQLException ex){   
         JOptionPane.showMessageDialog(null, "Terjadi error pada saat perbaru data");
        } 
    }
    
    public static int jual(){
        
        query = "UPDATE barang SET "
            + "stok=stok-? "
            + "WHERE barangID=?";
        
        try {
            connection = core.connect.getConnection();
            PreparedStatement statement = (PreparedStatement) connection.prepareStatement(query);
            statement.setInt(1, stok);
            statement.setInt(2, barangID);
            System.out.println(stok + " " + barangID);
            return statement.executeUpdate();   
          
        } catch (SQLException ex){
           return 0;
        } 
    }
    
    public static void Delete()
    {
        query = "DELETE FROM barang WHERE barangID=?";   
        try 
        {
            connection = core.connect.getConnection();
            
            PreparedStatement statement = (PreparedStatement) connection.prepareStatement(query);
            statement.setInt(1, barangID);
            if(statement.executeUpdate() == 1) {JOptionPane.showMessageDialog(null, "Data terhapus.");}
            else if(statement.executeUpdate() == 0)
            {JOptionPane.showMessageDialog(null, "Data gagal terhapus.");}
            
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Terjadi error pada saat hapus data");
        } 
    }
    
    public static ResultSet getByID(int id)
    {
        query = "SELECT * FROM barang WHERE barangID='"+ id +"'";   
        try 
        {
           connection = core.connect.getConnection();
           Statement statement = (Statement) connection.createStatement();
           ResultSet result = statement.executeQuery(query);
           return result;
        } catch (SQLException ex){
            return null;
        } 
    }
    
    public static ResultSet getStok(int id)
    {
        query = "SELECT stok FROM barang WHERE barangID='"+ id +"'";   
        try 
        {
           connection = core.connect.getConnection();
           Statement statement = (Statement) connection.createStatement();
           ResultSet result = statement.executeQuery(query);
           return result;
        } catch (SQLException ex){
            return null;
        } 
    }
    
    public  static void Show(DefaultTableModel tableModel)
    {
        
        try 
        {
           connection = core.connect.getConnection();
           
           query = "SELECT * FROM barang";      
           Statement statement = (Statement) connection.createStatement();
           ResultSet result = statement.executeQuery(query);
            
           while (result.next()){
                Object[] gung = new Object[4];
                gung[0] = result.getString("barangID");
                gung[1] = result.getString("nama");
                gung[2] = result.getString("stok");
                gung[3] = result.getString("hargaSatuan");
                tableModel.addRow(gung);
            }
           
            result.close();
            statement.close();
           
        }catch (SQLException ex){    
          System.out.println("Terjadi error pada saat tampil data");
        }
        
    }
    
    
     public  static void FilterbyID(DefaultTableModel tableModel)
    {
        
        try 
        {
           connection = core.connect.getConnection();
           
            query = "SELECT * FROM barang WHERE barangID LIKE '%"+ barangID +"%'";    
           Statement statement = (Statement) connection.createStatement();
           ResultSet result = statement.executeQuery(query);
            
           while (result.next()){
               Object[] gung = new Object[4];
                gung[0] = result.getString("barangID");
                gung[1] = result.getString("nama");
                gung[2] = result.getString("stok");
                gung[3] = result.getString("hargaSatuan");
                tableModel.addRow(gung);
            }
           
            result.close();
            statement.close();
           
        }catch (SQLException ex){    
          System.out.println("Terjadi error pada saat tampil data");
        }
        
    }
}
