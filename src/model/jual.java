/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Hydra
 */
public class jual {
   private static Connection connection;
    private static String query;
    
    public static Integer jualID;
    public static Integer barangID;
    public static String nama;
    public static Integer jumlah;
    public static Float harga;
    public static Date waktu;
    
     public static void save(){

        
        query = "INSERT INTO jual"
                + "(jualID, nama, barangID, jumlah, harga, waktu)"
                + "VALUES (null,?,?,?,?,?)";
        try {
          
            
          connection = core.connect.getConnection();
          
          PreparedStatement statement= (PreparedStatement) connection.prepareStatement(query);
          statement.setString(1, nama);
          statement.setInt(2, barangID);
          statement.setInt(3, jumlah);
          statement.setFloat(4, harga);
          statement.setDate(5, new java.sql.Date(waktu.getTime()));
          
          
          Integer stokBarang = 0;
          ResultSet result = barang.getStok(barangID);
          while(result.next())
          {              
              stokBarang = Integer.parseInt(result.getString("stok"));
          }
          
//          System.out.println(nama + " "+ barangID + " " + jumlah + " " + harga + " " + stokBarang + " " + (stokBarang != 0));    
              if(barang.getByID(barangID).next() 
                      && barang.getStok(barangID).next() 
                      && stokBarang != 0) {
                  
                  barang.barangID = barangID;
                  barang.stok = jumlah;
                  if(barang.jual() == 1){
                      if(statement.executeUpdate() == 1){JOptionPane.showMessageDialog(null, "Data tersimpan.");}
                      else if(statement.executeUpdate() == 0) {JOptionPane.showMessageDialog(null, "Data gagal tersimpan.");}
                  }else{
                      JOptionPane.showMessageDialog(null, "Data barang gagal di perbarui stoknya ");
                  }
                  
              }else {
                  JOptionPane.showMessageDialog(null, "Barangnya gak ada. ");
              }
          
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Terjadi error pada saat input data");
        } 
    }
     
     public static void Delete()
    {
        query = "DELETE FROM jual WHERE jualID=?";   
        try 
        {
            connection = core.connect.getConnection();
            
            PreparedStatement statement = (PreparedStatement) connection.prepareStatement(query);
            statement.setInt(1, jualID);
            if(statement.executeUpdate() == 1) {JOptionPane.showMessageDialog(null, "Data terhapus.");}
            else if(statement.executeUpdate() == 0)
            {JOptionPane.showMessageDialog(null, "Data gagal terhapus.");}
            
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Terjadi error pada saat hapus data");
        } 
    }
     
     public  static void Show(DefaultTableModel tableModel)
    {
        
        try 
        {
           connection = core.connect.getConnection();
           
           query = "SELECT * FROM jual";      
           Statement statement = (Statement) connection.createStatement();
           ResultSet result = statement.executeQuery(query);
            
           while (result.next()){
                Object[] gung = new Object[6];
                gung[0] = result.getString("jualID");
                gung[1] = result.getString("barangID");
                gung[2] = result.getString("nama");
                gung[3] = result.getString("jumlah");
                gung[4] = result.getString("harga");
                gung[5] = result.getString("waktu");
                tableModel.addRow(gung);
            }
           
            result.close();
            statement.close();
           
        }catch (SQLException ex){    
          System.out.println("Terjadi error pada saat tampil data");
        }
        
    }
}
