-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 24, 2018 at 03:48 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sekolah`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `barangID` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `hargaSatuan` float DEFAULT NULL,
  PRIMARY KEY (`barangID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`barangID`, `nama`, `stok`, `hargaSatuan`) VALUES
(101, 'Kemeja', 30, 15000),
(102, 'Tepung terigu', 4, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `beli`
--

CREATE TABLE IF NOT EXISTS `beli` (
  `beliID` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `waktu` date NOT NULL,
  `barangID` int(11) DEFAULT NULL,
  PRIMARY KEY (`beliID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21341 ;

--
-- Dumping data for table `beli`
--

INSERT INTO `beli` (`beliID`, `nama`, `jumlah`, `harga`, `waktu`, `barangID`) VALUES
(21337, 'Kemeja', 20, 100000, '2018-06-22', 101),
(21338, 'Tepung terigu', 5, 30000, '2018-06-22', 102),
(21340, 'Kemeja', 20, 100000, '2018-06-22', 101);

-- --------------------------------------------------------

--
-- Table structure for table `jual`
--

CREATE TABLE IF NOT EXISTS `jual` (
  `jualID` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `waktu` date DEFAULT NULL,
  `barangID` int(11) DEFAULT NULL,
  PRIMARY KEY (`jualID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `jual`
--

INSERT INTO `jual` (`jualID`, `nama`, `jumlah`, `harga`, `waktu`, `barangID`) VALUES
(13, 'Kemeja', 10, 100000, '2018-06-22', 101);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `no_induk` varchar(4) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `nama_wali` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  PRIMARY KEY (`no_induk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`no_induk`, `nama`, `nama_wali`, `tanggal_lahir`, `telepon`, `alamat`) VALUES
('S001', 'Ahmad Yusuf', 'Yusuf', '2002-03-11', '081202834957', 'Jl. Perintis Kemerdekaan No.20'),
('S002', 'Intan Ali', 'Ali', '2002-01-23', '081143205833', 'Jl. Urip Sumoharjo No.12'),
('S003', 'Rahmad Syahrul', 'Syahrul', '2003-04-14', '081340673920', 'Jl. Tentara Pelajar No.3'),
('S004', 'Ridwan Rahman', 'Rahman', '2002-09-18', '081186746305', 'Jl. AP. Pettarani No.2'),
('S005', 'Putri Nur Hakim', 'Hakim', '2001-09-29', '081195826486', 'Jl. Sultan Alauddin No.5');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userID` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID`, `username`, `password`, `status`) VALUES
(1, 'agung', '', 'Admin'),
(2, 'asd', 'asd', 'Admin'),
(3, 'admin', 'admin', 'Admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
